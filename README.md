# openmediavault

Base install and default config for OpenMediaVault Fedaykin.
No config beyond initial package install and default OMV username/password.

Tested with Debian Squeeze and Wheezy. 


Notes
-----
  * Tested on Debian Squeeze and Wheezy
  * Dependency on nrvale0/postfix -- simple module for ensureing Postfix is installed.


ToDo
----
  * add parameter for setting non-default admin password?
  * DRTs for setting up volumes, exports, etc?
  * verify functionality on Raspbian.


License
-------
Apache License, Version 2.0


Contact
-------
Nathan Valentine - nrvale0@gmail.com


Support
-------

Please log tickets and issues at [the project's site](http://github.com/nrvale0/puppet-openmediavault).
