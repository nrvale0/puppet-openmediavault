class openmediavault::params {
  case $::osfamily {
    'debian': {}
    default: {fail("OS family ${::osfamily} not supported!")}
  }
}
