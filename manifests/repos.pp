class openmediavault::repos {
  include openmediavault::params

  $deb_mm_repo = "http://www.deb-multimedia.org"
  $deb_omv_repo = "http://packages.openmediavault.org/public"

  class { 'apt':
    always_apt_update => true,
  }
 
  apt::source { 'Deb_multimedia_repo':
    ensure => present,
    location => $deb_mm_repo,
    release => $lsbdistname,
    repos => 'main non-free',
    include_src => false,
    required_packages => ['deb-multimedia-keyring'],
    key => '5D3877A7',
    key_server => 'pgp.mit.edu',
  }

  apt::source { 'OpenMediaVault_repo':
    ensure => present,
    location => $deb_omv_repo,
    release => 'fedaykin',
    repos => 'main',
    include_src => false,
    key_server => 'pgp.mit.edu',
  } -> exec { 'OpenMediaVault repo key':
    path => ['/bin','/sbin','/usr/bin','/usr/sbin'],
    command => 'wget -q -O- http://packages.openmediavault.org/public/archive.key | apt-key add -',
    unless => 'apt-key list | grep 2EF35D13',
  } -> exec { 'apt-get update':
    path => ['/bin','/sbin','/usr/bin','/usr/sbin'],
    command => 'apt-get update',
  }

#  } -> apt::key { 'openmediavault-keyring':
#
#    ensure => present,
#    key => '2EF35D13',
#  } -> 

}
