class openmediavault {
  include openmediavault::params

  require postfix
  require openmediavault::repos

  package { 
   ['openmediavault','openmediavault-iscsitarget','openmediavault-lvm2']:
    ensure => present,
  }

  service { 'openmediavault': 
    ensure => running,
    enable => true,
  } 

  # For reasons that are not entirely clear to me, it is sometimes
  # necessary to restart SSH and Apache after an OMV install.
  # sshd will not accept connections and Apache will sometimes 
  # segfault until restarted.
 
  Exec { path => ['/bin','/sbin','/usr/bin','/usr/sbin'], }
  exec { 'post-OMV sshd restart': 
    command => 'service ssh restart',
    subscribe => Package['openmediavault'],
    refreshonly => true,
  }
  exec { 'post-OMV Apache restart':
    command => 'service apache2 restart',
    subscribe => Package['openmediavault'],
    refreshonly => true,
  }
}
